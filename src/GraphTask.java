import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * 
 * Koostada meetod etteantud graafi n astme leidmiseks (n>=0) kasutades korrutamist.
 * Kui lähtegraafis G leidub (lühim) tee pikkusega n tipust a tippu b, siis tulemusgraafis G astmel n on kaar ab.
 * 
 * http://enos.itcollege.ee/~jpoial/algoritmid/vormistus.html
 * https://en.wikipedia.org/wiki/Adjugate_matrix
 * http://mathworld.wolfram.com/GraphPower.html
 * http://www.cs.yale.edu/homes/aspnes/pinewiki/GraphTheory.html
 * http://jgrapht.org/javadoc/org/jgrapht/graph/AbstractGraph.html
 */
public class GraphTask {
	
	//Kasutaja peab kirjutama p�rast annaAste v�rdusm�rki positiivse t�isarvu, 
	//et anda programmile ette see aste, 
	//millel tahetakse graaf saada
	public int annaAste = 2;

	//Samuti võib kasutaja ka ette anda mitu tippu ja kaart ta esialgsele graafile tahab- esialgne graaf genereeritakse suvaliselt
	public int annaTippudeArv = 3;
	public int annaKaarteArv = 2;
	
   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();      
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph (" ");
      
    if (annaKaarteArv < annaTippudeArv-1 || annaKaarteArv > annaTippudeArv*(annaTippudeArv-1)/2){
          throw new IllegalArgumentException("Võimatu kaarte arv: " + annaKaarteArv);
    }
      
    if(annaTippudeArv == 0 && annaKaarteArv == 0 || annaKaarteArv == 1 && annaTippudeArv == 0) {
    	throw new IllegalArgumentException ("Graafil peavad olema mingisugusedki parameetrid. Vähemalt üks tipp");
    }
      
  	if (annaAste < 0) {
		throw new IllegalArgumentException ("Astendaja peab olema >= 0");
	}
      
      if (annaTippudeArv <= annaAste) {
    	  throw new IllegalArgumentException ("Astendaja peab olema väksem(n-1) kui tippude arv n");
	}
    
     if (annaAste == 1) {
    	  System.out.println ("Astmel 1 on graaf järgmine: ");
    	  g.createRandomSimpleGraph (annaTippudeArv, annaKaarteArv);
    	  System.out.println (g);
	}
        
     if (annaAste > 1) {
    	 System.out.println ("Suvaline lihtgraaf on järgmine: ");
    	 g.createRandomSimpleGraph (annaTippudeArv, annaKaarteArv);
    	 System.out.println (g);
    	 
    	 int[][] m = g.getDegree();
    	 System.out.println ("Astmel " + annaAste + " on lihtgraafi maatriks järgmine:");
    	 g.printMatrix(m);
     }
     
     if (annaAste == 0) {
    	 System.out.println ("Astmel 0 on graaf järgmine: ");
    	 g.createRandomSimpleGraph (annaTippudeArv, annaKaarteArv);
    	 System.out.println (g);
     }
     
   }

   
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         
         if (annaAste == 0) {
        	 Vertex v = first;
        	 while (v != null) {
        	 sb.append (v.toString());
        	 sb.append (nl);
        	 v = v.next;
        	 }
		} else {
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
		} 
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }
      
      /**
       * Matrix multiplication method. taken from http://stackoverflow.com/questions/17623876/matrix-multiplication-using-arrays and modified to needs
       * @param m1 Multiplicand
       * @param m2 Multiplier
       * @return Product
       **/
          public int[][] multiplyByMatrix(int[][] m1, int[][] m2) {
              int m1ColLength = m1[0].length; // m1 columns length
              int m2RowLength = m2.length;    // m2 rows length
              if(m1ColLength != m2RowLength) return null; // matrix multiplication is not possible
              int mRRowLength = m1.length;    // m result rows length
              int mRColLength = m2[0].length; // m result columns length
              int[][] mResult = new int[mRRowLength][mRColLength];
              for(int i = 0; i < mRRowLength; i++) {         // rows from m1
                  for(int j = 0; j < mRColLength; j++) {     // columns from m2
                      for(int k = 0; k < m1ColLength; k++) { // columns from m1
                    	  mResult[i][j] += m1[i][k] * m2[k][j];
                      }
                  }
              }
              return mResult;
          }    
          
          /**
           * Create a matrix for a connected simple (undirected, no loops, no multiple
           * arcs) random graph with n vertices and m edges.
           * @return Matrix
           **/  
        public int[][] getDegree(){
        	
        	int[][] adjMatrix = createAdjMatrix();
        	int[][] destMatrix = createAdjMatrix();
        	
        	for (int i = 0; i < (annaAste+1); i++) {   		
        	destMatrix = multiplyByMatrix(adjMatrix, destMatrix);
        	}
        	
        	for (int i = 0; i < destMatrix.length; i++) {
				for (int j = 0; j < destMatrix.length; j++) {
					if (i == j) destMatrix[i][j] = 0;
					if (destMatrix[i][j] > 0) destMatrix[i][j] = 1;
				}
        	}
        	
        	return destMatrix;	
        }   
        
        /**
         * Matrix printing method. Taken from http://stackoverflow.com/questions/5061912/printing-out-a-2-d-array-in-matrix-format
         * @param m matrix
         **/
        public void printMatrix(int[][] m){
            try{
                int rows = m.length;
                int columns = m[0].length;
                String str = "|\t";

                for(int i=0;i<rows;i++){
                    for(int j=0;j<columns;j++){
                        str += m[i][j] + "\t";
                    }
                    System.out.println(str + "|");
                    str = "|\t";
                }

            }catch(Exception e){System.out.println("Matriks on tühi!!");}
        }
        
        
        
      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
                  
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
         
      }


   }

} 

